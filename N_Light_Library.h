#define MASTER_ID_MASTER 0x01
#define MASTER_ID_SLAVE  0x02

#define GET         0xA0
#define TOGGLE      0xA1
#define SET         0xA2

#define INIT        0x21


#define FT          0x60
#define DT          0x61
#define ST          0x62
#define ID          0x65
#define STOP_ID     0x66
#define STATUS      0x6F



#define DEVICE_NAME 0x82
#define SN          0x8A
#define BOTH        0x8B

#define DALI_FT         0xE3
#define DALI_DT         0xE4
#define DALI_ST         0xE5
#define DALI_ID         0xF0


#define DALI_QUERY_BATTERY  0xF1
#define DALI_QUERY_EM       0xFA
#define DALI_QUERY_FS       0xFC
#define DALI_QUERY_ES       0xFD


